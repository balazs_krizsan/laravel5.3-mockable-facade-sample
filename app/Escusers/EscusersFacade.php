<?php

namespace App\Escusers;

use Illuminate\Support\Facades\Facade;

class EscusersFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
    	return \App\Escusers\Escusers::class;
    }

}

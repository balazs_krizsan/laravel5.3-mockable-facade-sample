<?php

namespace App\Escusers;

use Illuminate\Support\ServiceProvider;

class UsersServiceProvider extends ServiceProvider
{

    protected $defer = true;

    public function register()
    {
        $this->app->bind(\App\Escusers\Escusers::class, function()
        {
            return new \App\Escusers\Escusers;
        });
    }

    public function provides()
    {
        return [
            \App\Escusers\Escusers::class,
        ];
    }
}
